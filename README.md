# Miscreated Log Processor
This script will process Miscreated logs and store the results in an sqlite3 database *-or-* output the results from a single file in JSON or CSV formats. Miscreated Log Processor (*MLP*) has been newly re-written in Python to improve speed and reliability.

NOTE: *This log processor is designed for use only with damage logs created since the release of Miscreated Update #55*. Damage logs created prior to Update #55 will ***not likely*** be processed correctly. *Chat logs* created prior to Update #55 should be consumed without issue.

## Features
* Parse chat logs by server.
* Parse damage logs by server.
* Can handle logs from multiple servers.
* Can handle out of order processing of logs.
* Can re-process updated logs, continuing from last record processed. This can be helpful if a log is updated after the initial processing.

## Requirements
*MLP* was created on Ubuntu 18.04; only limited testing has been done on Windows - I expect it to work fine, it worked great for me, but your mileage may vary. You will need Python3 to run *MLP*.

## Releases
Please only download the release versions from here: https://gitlab.com/spafbi/miscreated-log-processor/tags.

## Notes
* Modify the `config.json` file to reflect the desired file locations. An
example config for a single server would look like this:

```
{
  "Spafbi's Sneaky Old Bastards": {
    "db_file": "data/ssob.db",
    "log_dir": "logs/ssob"
  }
}
```

Two servers would look like this:

```
{
  "Spafbi's Sneaky Old Bastards": {
    "db_file": "data/ssob.db",
    "log_dir": "logs/ssob"
  },
  "Spafbi's Castles": {
    "db_file": "data/castles.db",
    "log_dir": "logs/castles"
  }
}
```

Of course you could have as many servers in there as you like... just keep
adding them.

## How to run
Command line, only. Don't forget to configure the `config.json` file, specifying the location of your log files, and make sure the target database directory exists and is writable.

### Windows
Assuming Python3 is in your path and *MLP* is in `C:\MLP\`, open a command prompt and execute:
```
python C:/mlp/mlp.py
```

### Linux
Assuming the `mlp.py` file is executable, from the `mlp` directory, execute:
```
$ ./mlp.py
```
If `mlp.py` file is not executable, from the `mlp` directory, execute:
```
$ python3 ./mlp.py
```

## Processing individual files
*MLP* may be used to process individual files and output the results in either
JSON or CSV format - you will just need to specify the target log file, and the desired output format. Just run `mlp.py -h` for more info.
