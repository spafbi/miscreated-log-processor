#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# title           : mlp.py
# description     : Used to process Miscreated server logs.
# author          : Chris Snow (a.k.a. Spafbi)
# usage           : see ./mlp.py -h for usage
# python_version  : 3.6.5
# ==============================================================================
import argparse
import csv
import json
import os
import sqlite3
import sys
from pprint import pprint
from lib import mlpdb as mlpdb
from lib import parsechat as parsechat
from lib import parsedamage as parsedamage


def insert_chat(db, filedate, values):
    """ insert a chat entry into the database
    :param db: database connection object
    :param filedate: presumably the date of the file being processed.
    :param values: dictionary containing a processed chat log entry data.
    """
    c = db.conn.cursor()
    timestamp = filedate + ' ' + values['timestamp']
    player_name = values['player_name']
    steamID64 = values['steam_id']
    message = values['message']
    iv = (steamID64, timestamp, player_name, message)
    sql = 'INSERT INTO chat_log ("steam_id","timestamp","player_name","message") VALUES (?,?,?,?)'
    try:
        c.execute(sql, iv)
    except sqlite3.Error as e:
        print(e)


def insert_damage(db, filedate, values):
    """ insert a damage entry into the database
    :param db: database connection object
    :param filedate: presumably the date of the file being processed.
    :param values: dictionary containing a processed damage log entry data.
    """
    c = db.conn.cursor()
    timestamp = filedate + ' ' + values.get('timestamp')
    rawtime = values.get('rawtime')
    microseconds = values.get('microseconds')
    distance = values.get('distance')
    dmgBodyMult = values.get('dmgBodyMult')
    dmgEqMult = values.get('dmgEqMult')
    dmgFactionMult = values.get('dmgFactionMult')
    dmgTotal = values.get('dmgTotal')
    dmgUnscaled = values.get('dmgUnscaled')
    driverFaction = values.get('driverFaction')
    driverName = values.get('driverName')
    driverSteamID = values.get('driverSteamID')
    eventType = values.get('eventType')
    headshot = values.get('headshot')
    hitType = values.get('hitType')
    kill = values.get('kill')
    melee = values.get('melee')
    part = values.get('part')
    partdesc = values.get('partdesc')
    partnum = values.get('partnum')
    rawdamage = values.get('rawdamage')
    relspeedsq = values.get('relspeedsq')
    shooterFaction = values.get('shooterFaction')
    shooterName = values.get('shooterName')
    shooterPos = values.get('shooterPos')
    shooterPosX = values.get('shooterPosX')
    shooterPosY = values.get('shooterPosY')
    shooterPosZ = values.get('shooterPosZ')
    shooterSteamID = values.get('shooterSteamID')
    targetFaction = values.get('targetFaction')
    targetName = values.get('targetName')
    targetPos = values.get('targetPos')
    targetPosX = values.get('targetPosX')
    targetPosY = values.get('targetPosY')
    targetPosZ = values.get('targetPosZ')
    targetSteamID = values.get('targetSteamID')
    timeOfDay = values.get('timeOfDay')
    vehicle = values.get('vehicle')
    weapon = values.get('weapon')

    iv = (timestamp, rawtime, microseconds, distance, dmgBodyMult, dmgEqMult,
          dmgFactionMult, dmgTotal, dmgUnscaled, driverFaction, driverName,
          driverSteamID, eventType, headshot, hitType, kill, melee, part,
          partdesc, partnum, rawdamage, relspeedsq, shooterFaction,
          shooterName, shooterPos, shooterPosX, shooterPosY, shooterPosZ,
          shooterSteamID, targetFaction, targetName, targetPos, targetPosX,
          targetPosY, targetPosZ, targetSteamID, timeOfDay, vehicle, weapon)
    sql = """ INSERT INTO damage_log (
          "timestamp", "rawtime", "microseconds", "distance", "dmgBodyMult",
          "dmgEqMult", "dmgFactionMult", "dmgTotal", "dmgUnscaled",
          "driverFaction", "driverName", "driverSteamID", "eventType", "headshot", "hitType",
          "kill", "melee", "part", "partdesc", "partnum", "rawdamage",
          "relspeedsq", "shooterFaction", "shooterName", "shooterPos", "shooterPosX",
          "shooterPosY", "shooterPosZ", "shooterSteamID", "targetFaction", "targetName",
          "targetPos", "targetPosX", "targetPosY", "targetPosZ",
          "targetSteamID", "timeOfDay", "vehicle", "weapon")
          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""
    try:
        c.execute(sql, iv)
    except sqlite3.Error as e:
        print(iv)
        print(e)
    return


def print_errors(warnings):
    """ print values from the warnings dictionary.
    :param warnings: warnings dictionary
    """
    if len(warnings) > 0:
        print('ERROR(S):')
        for n, e in warnings.items():
            pprint(e)


def process_file(fname):
    fnamesnippet = os.path.basename(fname)[0:7]
    if fnamesnippet in ('chatlog', 'damagel'):
        with open(fname) as f:
            content = f.readlines()
            content = [x.strip() for x in content]
            linecount = 0
            data = dict()
            if fnamesnippet == 'chatlog':
                for line in content:
                    data[linecount] = parsechat.parse_chat_line(line)
                    linecount += 1
            elif fnamesnippet == 'damagel':
                for line in content:
                    data[linecount] = parsedamage.parse_damage_line(line)
                    linecount += 1
            return data


def output_chat_csv(data):
    first = 1
    for key, value in data.items():
        output = csv.DictWriter(
            sys.stdout, value.keys(), lineterminator='\r\n')
        if first:
            output.writeheader()
            first = 0
        else:
            output.writerow(value)


def output_damage_csv(old_data):
    data = dict()
    for key, value in old_data.items():
        data[key] = {"timestamp": value.get('timestamp'),
                     "rawtime": value.get('rawtime'),
                     "microseconds": value.get('microseconds'),
                     "timeOfDay": value.get('timeOfDay'),
                     "distance": value.get('distance'),
                     "dmgBodyMult": value.get('dmgBodyMult'),
                     "dmgEqMult": value.get('dmgEqMult'),
                     "dmgFactionMult": value.get('dmgFactionMult'),
                     "dmgTotal": value.get('dmgTotal'),
                     "dmgUnscaled": value.get('dmgUnscaled'),
                     "driverFaction": value.get('driverFaction'),
                     "driverName": value.get('driverName'),
                     "driverSteamID": value.get('driverSteamID'),
                     "eventType": value.get('eventType'),
                     "headshot": value.get('headshot'),
                     "hitType": value.get('hitType'),
                     "kill": value.get('kill'),
                     "melee": value.get('melee'),
                     "projectile": value.get('projectile'),
                     "part": value.get('part'),
                     "partdesc": value.get('partdesc'),
                     "partnum": value.get('partnum'),
                     "rawdamage": value.get('rawdamage'),
                     "relspeedsq": value.get('relspeedsq'),
                     "shooterFaction": value.get('shooterFaction'),
                     "shooterName": value.get('shooterName'),
                     "shooterPos": value.get('shooterPos'),
                     "shooterPosX": value.get('shooterPosX'),
                     "shooterPosY": value.get('shooterPosY'),
                     "shooterPosZ": value.get('shooterPosZ'),
                     "shooterSteamID": value.get('shooterSteamID'),
                     "targetFaction": value.get('targetFaction'),
                     "targetName": value.get('targetName'),
                     "targetPos": value.get('targetPos'),
                     "targetPosX": value.get('targetPosX'),
                     "targetPosY": value.get('targetPosY'),
                     "targetPosZ": value.get('targetPosZ'),
                     "targetSteamID": value.get('targetSteamID'),
                     "vehicle": value.get('vehicle'),
                     "weapon": value.get('weapon')}

    first = 1
    for key, value in data.items():
        output = csv.DictWriter(sys.stdout, value.keys(), lineterminator='\r')
        if first:
            output.writeheader()
            first = 0
        else:
            output.writerow(value)


def main():
    description = 'This script will parse Miscreated chat and damage logs for use in spreadsheets, databases, and other applications. If parsing a single log file, results will be sent to stdout if a server is not specified.'
    epilog = 'Settings from the config.json file will be used if run without switches.'
    parser = argparse.ArgumentParser(
        'mis-log - Miscreated Log Processor', description, epilog)
    parser.add_argument('-l', '--log', metavar='[log_filename]', type=str,
                        required=False, help="Full path to a chat or damage log file",
                        default=-1)
    parser.add_argument('-f', '--format', metavar='<format>', type=str,
                        required=False, help="Output format " +
                        "[csv,json]")
    parser.add_argument('-d', '--debug', action='store_true',
                        help="Enable debugging")
    args = parser.parse_args()
    logfile = args.log
    output_format = args.format
    debugging = args.debug

    cmdline = dict()
    errors = dict()
    warnings = dict()

    if (not os.path.isfile(logfile)) & (logfile != -1):
        errors[len(errors) + 1] = '  - The specified input log file does not exist.'
    elif args.log != -1:
        cmdline['log_file'] = logfile
    else:
        cmdline['log_file'] = None

    if args.format is not None:
        if args.format in ('csv', 'json'):
            cmdline['format'] = args.format
        else:
            errors[len(errors) + 1] = '  - Invalid format (' + \
                args.format + ') specified.'

    if debugging:
        print('Argument values:', args)
        print('Arguments from command line:', cmdline)

    if len(errors):
        print_errors(errors)

    if logfile != -1:
        result = process_file(logfile)
        if output_format == 'csv':
            basename = os.path.basename(logfile)
            fnamesnippet = basename[0:7]
            if fnamesnippet == 'chatlog':
                output_chat_csv(result)
            elif fnamesnippet == 'damagel':
                output_damage_csv(result)
            else:
                print('ERROR: Filename must begin with chatlog or damagelog')
                exit(1)
        else:
            print(json.dumps(result))
        exit(0)

    json_data = open('config.json').read()
    config = json.loads(json_data)

    for server, data in config.items():

        if debugging:
            print('Processing logs for' + server + '...')

        db_file = data.get('db_file')
        log_dir = data.get('log_dir')

        # Setup DB
        if db_file == None:
            warnings[len(
                warnings) + 1] = "No 'db_file' was defined in the " +\
                                 "config.json file for server: " + server +\
                                 " No action taken."
            exit(1)
        db = mlpdb.DatabaseConnection(db_file)
        c = db.conn.cursor()

        # Process files, if any.
        if os.path.isdir(log_dir):
            for filename in os.listdir(log_dir):
                fname = os.path.join(log_dir, filename)
                basename = os.path.basename(fname)
                fnamesnippet = basename[0:7]
                if fnamesnippet not in ('chatlog', 'damagel'):
                    continue
                datestart = basename.find('_') + 1
                filedate = basename[datestart:datestart + 10]
                bn = (basename,)
                c.execute(
                    'SELECT last_row FROM file_history WHERE filename = ?', bn)
                result = c.fetchone()
                if result is None:
                    startrow = -1
                    sr = (basename, -1)
                    sql = 'INSERT INTO file_history VALUES (?,?)'
                    try:
                        c.execute(sql, sr)
                    except Error as e:
                        print(e)
                else:
                    startrow = result["last_row"] + 1

                if debugging:
                    print("Starting at row: " + str(startrow))

                result = process_file(fname)
                for value in result.items():
                    if value[0] < startrow:
                        continue
                    else:
                        if fnamesnippet == 'chatlog':
                            insert_chat(db, filedate, value[1])
                        elif fnamesnippet == 'damagel':
                            insert_damage(db, filedate, value[1])
                if value[0] != None:
                    fh = (value[0], basename)
                    sql = 'UPDATE file_history SET last_row = ? WHERE filename = ?'

                    try:
                        c.execute(sql, fh)
                    except Error as e:
                        print(e)
                    if debugging:
                        if startrow == -1:
                            rows_added = value[0] + 1
                        else:
                            rows_added = value[0] + 1 - startrow
                        print(basename, 'new records:', rows_added)
                    value = (None,)
        db.commit()
        db.close()

        if len(warnings):
            print_errors(warnings)


if __name__ == '__main__':
    main()
