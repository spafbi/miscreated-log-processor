#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# title           : parsedamage.py
# description     : Used to parse Miscreated damage log entries
# author          : Chris Snow (a.k.a. Spafbi)
# usage           : use the parse_damage_line method -or-
#                   echo '<entire log string>' | parsedamage.py
#                   parsedamage.py < damagelog_2017-12-31.txt
# python_version  : 3.6.5
# ==============================================================================


import json
import sys
from pprint import pprint


def parse_damage_line(line):
    '''
    Parse a line from the Miscrated damage log
    :param string: a line from a Miscreated damage log
    :return: a dictionary of the line's fields and values
    '''
    # remove carriage and line returns
    line = line.rstrip('\r\n')
    d = dict()
    # raw timestamp
    d['rawtime'] = line[1:13].strip()
    # timestamp is in a fixed location on the line
    d['timestamp'] = line[1:9].strip()
    # microseconds portion of the timestamp the type of log entry
    d['microseconds'] = line[10:13].strip()
    # in-game time of day
    d['timeOfDay'] = line[21:26].strip()
    d['eventType'] = line[27:27 + line[27:].find(' - ')].strip()

    # ** (hit, collision, explosion, etc.) **
    # everything after the entry type
    chunk = line[line.find(' - ') + 3:]
    while (chunk.find(': "') > 0):
        field = chunk[:chunk.find(': "')].strip()
        remainder = chunk[chunk.find(': "') + 3:].strip()

        # ** extract values ** #
        # We have to use a different "find" string for fields using user-defined
        # player names
        if field == 'shooterName':
            if d['eventType'] == 'explosion':
                value = remainder[:remainder.find('", target')]
            else:
                value = remainder[:remainder.find('", shooter')]
            d['shooterName'] = value.strip()
        elif field == 'driverName':
            value = remainder[:remainder.find('", driver')]
            d['driverName'] = value.strip()
        elif field == 'targetName':
            if d['eventType'] == 'explosion':
                value = remainder[:remainder.find('", weapon')]
            else:
                value = remainder[:remainder.find('", target')]
            d['targetName'] = value.strip()
        elif field == 'projectile':
            value = remainder[:remainder.find('", ')].replace('ammo_', '')
            d['projectile'] = value.strip()
        elif field == 'damage':
            value = remainder[:remainder.find('", ')]
            d['rawdamage'] = value.strip()
            if value.find('x*') > 1:
                value = value.replace('x', '').replace('=', '*').strip()
                d['dmgUnscaled'], d['dmgEqMult'], d['dmgBodyMult'], \
                    d['dmgFactionMult'], d['dmgTotal'] = value.split('*')
            else:
                d['dmgTotal'] = value.strip()
        else:
            value = remainder[:remainder.find('", ')]
            d[field] = value.strip()

        # extract position components
        if field in ('targetPos', 'shooterPos'):
            d[field + 'X'], d[field + 'Y'], d[field + 'Z'] = value.split(',')

        # extract 'part' data
        if field == 'part':
            d['partnum'] = value[0:value.find('(')]
            d['partdesc'] = value[value.find(
                '(') + 1:value.find(')')].replace('Bip01 ', '').replace('Def_', '')

        # pops off the data we just processed
        chunk = remainder[remainder.find('", ') + 3:]
    return d


def main():
    '''
    Iterates through lines passed from stdin
    :return: JSON formatted results
    '''
    linecount = 0
    data = dict()
    for line in sys.stdin:
        data[linecount] = parse_damage_line(line)
        linecount += 1
    pprint(json.dumps(data))


if __name__ == '__main__':
    main()
