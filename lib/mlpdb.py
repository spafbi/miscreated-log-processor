#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# title           : db.py
# description     : Create database connections
# author          : Chris Snow (a.k.a. Spafbi)
# usage           : use as a module, only.
# python_version  : 3.6.5
# ==============================================================================

import sqlite3
from sortedcontainers import SortedDict


class DatabaseConnection:
    def __init__(self, db_file):
        try:
            self.conn = sqlite3.connect(db_file)
        except sqlite3.Error as e:
            self.conn = None
            print(e)
            return
        self.conn.row_factory = self.dict_factory
        self.create_tables()

    def close(self):
        self.conn.commit()
        self.conn.close()

    def commit(self):
        self.conn.commit()

    def dict_factory(self, cursor, row):
        d = {}
        for idx, col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    def create_damage_log_indexes(self):
        sql = """
        CREATE INDEX damage_log_idx
        ON damage_log (`driverSteamID`, `kill`, `shooterSteamID`, `targetName`, `targetSteamID`, `timestamp`, `timeofday`);
        """
        c = self.conn.cursor()
        c.execute(sql)

    def create_tables(self):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param table_name: this
        """
        tables = SortedDict()
        tables['chat_log'] = SortedDict({
            'auto_id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
            'steam_id': 'INTEGER',
            'timestamp': 'TEXT',
            'player_name': 'TEXT',
            'message': 'TEXT',
        })
        tables['damage_log'] = SortedDict({
            'auto_id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
            'distance': 'NUMERIC',
            'dmgBodyMult': 'NUMERIC',
            'dmgEqMult': 'NUMERIC',
            'dmgFactionMult': 'NUMERIC',
            'dmgTotal': 'NUMERIC',
            'dmgUnscaled': 'NUMERIC',
            'driverFaction': 'TEXT',
            'driverName': 'TEXT',
            'driverSteamID': 'INTEGER',
            'eventType': 'TEXT',
            'headshot': 'INTEGER',
            'hitType': 'TEXT',
            'kill': 'INTEGER',
            'melee': 'INTEGER',
            'microseconds': 'INTEGER',
            'part': 'TEXT',
            'partdesc': 'TEXT',
            'partnum': 'NUMERIC',
            'rawdamage': 'TEXT',
            'rawtime': 'TEXT',
            'relspeedsq': 'NUMERIC',
            'shooterFaction': 'TEXT',
            'shooterName': 'TEXT',
            'shooterPos': 'TEXT',
            'shooterPosX': 'NUMERIC',
            'shooterPosY': 'NUMERIC',
            'shooterPosZ': 'NUMERIC',
            'shooterSteamID': 'INTEGER',
            'targetFaction': 'TEXT',
            'targetName': 'TEXT',
            'targetPos': 'TEXT',
            'targetPosX': 'NUMERIC',
            'targetPosY': 'NUMERIC',
            'targetPosZ': 'NUMERIC',
            'targetSteamID': 'INTEGER',
            'timeOfDay': 'NUMERIC',
            'timestamp': 'TEXT',
            'vehicle': 'TEXT',
            'weapon': 'TEXT',
        })
        tables['file_history'] = SortedDict({
            'filename': 'TEXT NOT NULL',
            'last_row': 'INTEGER DEFAULT 0',
        })

        c = self.conn.cursor()
        c.execute("SELECT name FROM sqlite_master WHERE type='table';")
        results = c.fetchall()
        existing_tables = []
        for t in results:
            existing_tables.append(t['name'])
        for table in tables:
            if table in existing_tables:
                continue
            sql = "CREATE TABLE `%s` (" % (table)
            for idx, key in enumerate(tables[table]):
                if len(tables[table][key]) > 0:
                    sql = sql + '`' + key + '` ' + tables[table][key]
                    if idx < (len(tables[table]) - 1):
                        sql = sql + ', '
            sql = sql + ")"
            print(sql)
            c.execute(sql)
            if table == "damage_log":
                self.create_damage_log_indexes()
