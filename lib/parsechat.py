#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# title           : parsechat.py
# description     : Used to parse Miscreated chat log entries
# author          : Chris Snow (a.k.a. Spafbi)
# usage           : use the parse_chat_line method -or-
#                   echo '<entire log string>' | parsechat.py
#                   parsechat.py < chatlog_2017-12-31.txt
# python_version  : 3.6.5
# ==============================================================================


import json
import sys
from pprint import pprint


def parse_chat_line(line):
    '''
    Parse a line from the Miscrated chat log
    :param string: a line from a Miscreated chat log
    :return: a dictionary of the line's fields and values
    '''
    # remove carriage and line returns
    line = line.rstrip('\r\n')
    c = dict()
    # in a fixed location on the line
    c['timestamp'] = line[1:9]
    # in a fixed location on the line
    c['steam_id'] = line[20:37]
    # the playername starts at position 45, so find position of the end
    player_name_end = (line[45:].find('] [IP ')) + 45
    c['player_name'] = line[45:(line[45:].find('] [IP ')) + 45]
    # the string starting at the playername end + 6 positions for the start of
    # the IP
    remaining = line[player_name_end + 6:]
    # the IP address field contents up to the ':'
    c['ip'] = remaining[0:remaining.find(':')]
    # the message field
    c['message'] = remaining[remaining.find('] ') + 2:]
    return c


def main():
    '''
    Iterates through lines passed from stdin
    :return: JSON formatted results
    '''
    linecount = 0
    data = dict()
    for line in sys.stdin:
        data[linecount] = parse_chat_line(line)
        linecount += 1
    pprint(json.dumps(data))


if __name__ == '__main__':
    main()
