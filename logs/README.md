# Miscreated Log Processor
## Log file directory
You may choose to use this directory in which to store your log files. It's
what I do. This is, of course, fully configurable in your `config.json` file.
Using the included `config.json` file as a template, I would create a directory
in here for each server; following that example I would create `server` and `server2` directories and place the respective log files for each server in
these directories.
